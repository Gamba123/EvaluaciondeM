﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Evaluacion2.Startup))]
namespace Evaluacion2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
