﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Evaluacion2.Models
{
    public class EvaluacionContext : DbContext
    {
        public EvaluacionContext() : base("DefaultConnection")
        {

        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public System.Data.Entity.DbSet<Evaluacion2.Models.Instructor> Instructors { get; set; }

        public System.Data.Entity.DbSet<Evaluacion2.Models.Sede> Sedes { get; set; }

        public System.Data.Entity.DbSet<Evaluacion2.Models.Specialty> Specialties { get; set; }
    }
}