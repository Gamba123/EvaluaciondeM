﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion2.Models
{
    public class Specialty
    {
        [Key]
        public int idSpecialty { get; set; }

        [Required(ErrorMessage ="el campo es obligatorio")]
        [StringLength(30)]
        [Display(Name ="nombre de la especialidad")]
        public string name { get; set; }

        public virtual ICollection<Instructor> Instructors { get; set; }

    }
}