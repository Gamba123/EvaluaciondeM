﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion2.Models
{
    public class Sede
    {
        [Key]

        public int idsede { get; set; }

        [Required(ErrorMessage ="el campo es obligatorio")]
        [StringLength(30,ErrorMessage ="el campo {0} puede contener minimo{1} caracteres")]
        [Display(Name ="nombre de la sede")]

        public string headquarters { get; set; }

        public virtual ICollection<Instructor> Instructors { get; set; }

    }
}