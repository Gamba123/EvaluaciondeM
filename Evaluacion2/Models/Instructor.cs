﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion2.Models
{
    public class Instructor
    {
        [Key]

        public int idInstructor { get; set; }

        [Required(ErrorMessage = "El campo es obligatorio")]
        [StringLength(30, ErrorMessage = "el campo debe {0} puede contener maximo{1} caracteres")]
        [Display(Name = "nombre")]
        public string name { get; set; }

        [Required(ErrorMessage = "El campo es obligatorio")]
        [StringLength(30, ErrorMessage = "el campo debe {0} puede contener maximo{1} caracteres")]
        [Display(Name = "segundo nombre")]
        public string second_Name { get; set; }


        [Required(ErrorMessage = "El campo es obligatorio")]
        [StringLength(30, ErrorMessage = "el campo debe {0} puede contener maximo{1} caracteres")]
        [Display(Name = "Primer Apellido")]
        public string surname { get; set; }

        [Required(ErrorMessage = "El campo es obligatorio")]
        [StringLength(30, ErrorMessage = "el campo debe {0} puede contener maximo{1} caracteres")]
        [Display(Name = "segundo Apellido")]
        public string second_Surname { get; set; }

        [Required(ErrorMessage = "el campo es requerido")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression("^[_a-z0-9-]+(.[_a-z0-9-]+)*@(gmail.com|outlook.com)$", ErrorMessage = "El correo debe ser mi sena o sena")]
        [StringLength(100, ErrorMessage = "El campo {0} puede contener maximo {1} caracteres")]
        [Display(Name = "Mail")]
        public string Mail { get; set; }

        [Required(ErrorMessage = "el campo es requerido")]
        [StringLength(20, ErrorMessage = "El campo {0} puede contener máximo {1} caracteres")]
        [Display(Name = "edad")]
        public string age { get; set; }

        [Display(Name = "fecha de nacimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime birh_Date { get; set; }


        public int idSpecialty { get; set; }
        public int idsede { get; set; }

        public virtual Sede sede { get; set; }
        public virtual Specialty specialty { get; set; }

    }
}